#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function

from copy import copy, deepcopy
from pprint import pprint, pformat
import itertools
import datetime, random
from timeit import default_timer as timer
from timeout import timeout, TimeoutError

import os
import json


MODE = os.environ.get('TSY_MODE','debug')

ENV = os.environ.get('ENV','dev')


class RuntimeError(Exception):
    pass


class ClientError(Exception):
    pass


class InputError(ClientError):
    pass


class NoSolutionError(RuntimeError):
    pass


logfile = ''
if ENV == 'GAE':
    import logging
else:
    logfile = open('log.txt', 'a')

def log(message, level='info', prefix = '', type='', do_pformat=True, file = logfile):
    if ENV == 'prod':
        return
    # print(message)
    # return
    # if type.startswith('sched'):
    #     return

    if do_pformat and (isinstance(message,list) or isinstance(message,dict)):
        message = pformat(message)
    else:
        message = str(message)

    if level == 'warning':
      level = '! Warning !'
    if level == 'error':
      level = '!!! ERROR !!! '
    if level == 'debug' and MODE != 'debug':
        return


    # datetime.datetime.today().isoformat()
    if ENV == 'GAE':
        if level == 'warning':
          log_method = logging.warning
        if level == 'error':
          log_method = logging.error
        if level == 'debug':
          log_method = logging.debug
        else:
          log_method = logging.info

        log_method(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+' ('+level+')\t'+prefix+message)
        # if '\n' in message:
        #     method('\n')
    else:
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+' ('+level+')\t'+prefix+message, file=file)
        if '\n' in message:
            print('\n', file=file)

class StorageAbstract:
    def __init__(self, cargos = []):
        """
            cargos is assumed to be a list of Box objects
            it's transformed to dict like {cargo1_id: cargo1, cargo2_id: cargo2}
        """
        self._cargos = {}
        for cargo in cargos:
            self.add_cargo(cargo)

    def get_cargos(self, cargo):
        return self._cargos

    def add_cargo(self, cargo):
        self._cargos[cargo.id] = cargo

    def remove_cargo(self, cargo):
        del self._cargos[cargo.id]

    def get_cargos_for(self, target):
        result = set()
        for cargo in self._cargos.values():
            if cargo.target==target:
                result.add(cargo)

        if result:
            return result
        else:
            raise KeyError("there's no cargo for {}".format(target))

    def has_cargo_for(self, target):
        try:
            self.get_cargos_for(target)
            return True
        except KeyError:
            return False

class Cargo:
    def __init__(self, id, source, target, box_count):
        self.id = id
        self.source = source
        self.target = target
        self.box_count = box_count

    def to_json(self):
        return '#{} ({} boxes)'.format(self.id,self.box_count)

    def __repr__(self):
        return '#{} ({} boxes)'.format(self.id,self.box_count)

class Train:
    def __init__(self, id, *args, **kwargs):
        self.id = id

    def __hash__(self):
        return self.id

    def to_json(self):
        return 'train #' + str(self.id)

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, self.__class__):
            return self.id == other.id
        return NotImplemented

    def __ne__(self, other):
        """Define a non-equality test"""
        if isinstance(other, self.__class__):
            return not self.__eq__(other)
        return NotImplemented

    def __repr__(self):
        return 'train #{}'.format(self.id)

class StorageArea(StorageAbstract):
    # id = 0

    def __repr__(self):
        return 'storage area'

class Slot:
    def __init__(self, trains):
        self.revisits = [] # list of {train_id: N, cost: <revisit_cost>}
        self.moves = [] # set of dicts with fields source, target, type(direct/split begin/split end), cost
        self.cost=0 # total cost among all moves and revisits
        for train in trains:
            if trains.count(train) > 1:
                raise ValueError('cant create slot from {} because {} is duplicated'.format(trains,train))
        self.trains = trains # trains are expected to be a tuple

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return set(self.trains) == set(other.trains)
        return NotImplemented

    def __repr__(self):
        return '{}(${})'.format(repr(self.trains),self.cost)


class Schedule:
    def __init__(self, parent, last_slot, moves_to_perform = None):
        self.parent = parent
        self.last_slot = last_slot
        self.children = []

        if parent:
            self.storage_area = deepcopy(parent.storage_area)
            self.total_cost = parent.total_cost + last_slot.cost
            self.slot_count = parent.slot_count + 1
            self.trains_visited_ever = copy(parent.trains_visited_ever)
            self.trains_visited_once = copy(parent.trains_visited_once)
            self.trains_visited_twice = copy(parent.trains_visited_twice)

            for train in last_slot.trains:
                if train in self.trains_visited_twice:
                    raise ValueError('{} visited more than 2 times'.format(train))

                if train in self.trains_visited_once:
                    self.trains_visited_once.remove(train)
                    self.trains_visited_twice.add(train)
                else:
                   self.trains_visited_ever.add(train)
                   self.trains_visited_once.add(train)

            self.moves_info = deepcopy(parent.moves_info)
            for move in last_slot.moves:
                self._add_move(move)

        else:
            self.storage_area = StorageArea()
            self.total_cost = 0
            self.slot_count = 0
            self.moves_info = {move_key: Schedule.MOVE_NEW for move_key in moves_to_perform}
            self.trains_visited_once = self.trains_visited_twice \
                                     = self.trains_visited_ever = set()

    def get_all_slots(self):
        """ resursively takes all slots """
        # TODO:cache function output
        sched = self
        slots = []
        while sched.last_slot is not None:
            slots.append(sched.last_slot)
            sched = sched.parent
        return list(reversed(slots))

    def _can_move(self, source, target, move_type): # real source and real target
        if isinstance(source,StorageArea) or isinstance(target,StorageArea):
            raise ValueError('Storage area must NOT be passed as move source or target!!!')

        move_status = self.moves_info.get((source,target))
        if move_status is None:
            # log('can do {} move {}-{}: {}'.format(move_type,source,target,False), type='sched', prefix='  '*self.slot_count, level='debug')
            return False

        if move_status == self.MOVE_NEW:
            # if isinstance(source, StorageArea) and isinstance(target, Train):
            #     raise ValueError('You cant move cargo from {} to {} because move status is {}'.format(source,target,move_status))

            # log('can do {} move {}-{}: {}'.format(move_type,source,target,True), type='sched', prefix='  '*self.slot_count, level='debug')
            return True
        if move_status == self.MOVE_IN_PROCESS:
            if move_type != self.MOVE_TYPE_STORAGE_TRAIN:
                raise ValueError('you want to complete move but move type is '+move_type)

            if self.storage_area.has_cargo_for(target):
                # log('can do {} move {}-{}: {}'.format(move_type,source,target,True), type='sched', prefix='  '*self.slot_count, level='debug')
                return True
            else:
                raise ValueError('{} has no box for {}'.format(self.storage_area, target))

            # if isinstance(source, StorageArea) and isinstance(target, Train):
                # log('can move {}-{}: {}'.format(source,target,True), type='sched')
                # return True
            # else:
                # raise ValueError('If you want to finish \'in process\' move, source must be storage area and target must be train')
            #     log('can move {}-{}: {}'.format(source,target,False), type='sched')
            #     return False


        log('can move {}-{}: {}'.format(source,target,False), type='sched', level='debug')
        return False # if such move doesn't exist or it's already completed

    MOVE_NEW = 'new'
    MOVE_IN_PROCESS = 'in process' # split_begin
    MOVE_COMPLETED = 'completed'

    MOVE_TYPE_TRAIN_TRAIN = 'direct'
    MOVE_TYPE_TRAIN_STORAGE = 'split_begin'
    MOVE_TYPE_STORAGE_TRAIN = 'split_end'

    def _add_move(self, move):
        move_key = (move['source'], move['target'])
        move_status = self.moves_info.get(move_key)

        if move_status is None:
            raise ValueError('move '+ str(move_key)+' is absent in \'moves to perform\' list!')

        if move_status == self.MOVE_COMPLETED:
            raise ValueError('move '+ str(move_key)+' is already completed!')

        if move['type'] == self.MOVE_TYPE_TRAIN_TRAIN:
            self.moves_info[move_key] = self.MOVE_COMPLETED
        elif move['type'] == self.MOVE_TYPE_TRAIN_STORAGE:
            self.moves_info[move_key] = self.MOVE_IN_PROCESS
            self.storage_area.add_cargo(move['cargo'])
        elif move['type'] == self.MOVE_TYPE_STORAGE_TRAIN:
            # if we move cargo from storage area to train, we want to make sure that move status is is process
            # i.e. we want to make sure that this cargo was placed to storage area before by source train
            if not self.storage_area.has_cargo_for(move['target']):
                raise ValueError('{} has no cargo for {}'.format(self.storage_area, move['target']))

            if move_status == self.MOVE_IN_PROCESS:
                self.moves_info[move_key] = self.MOVE_COMPLETED
                self.storage_area.remove_cargo(move['cargo'])
            else:
                raise ValueError('You try do move {} but sched\'s move status is not completed!'.format(move_key))

        return self.moves_info[move_key]

    def get_train_targets(self, train, all_targets = False):
        result = set()
        for move_key,move_info in self.moves_info.items():
            if move_key[0] == train and (all_targets or move_info == self.MOVE_NEW):
                result.add(move_key[1])

        return result

    def get_train_sources(self, train, all_sources = False):
        result = set()
        for move_key,move_info in self.moves_info.items():
            if move_key[1] == train and (all_sources or move_info == self.MOVE_NEW):
                result.add(move_key[0])

        return result

    def there_is_cargo_for_train_in_next_slot(self, train, slot_trains):
        return self.get_train_sources(train).intersection(set(slot_trains))

    def __repr__(self):
        return '${}:{}'.format(self.total_cost, repr(self.get_all_slots()))

    def to_json(self):
        import inspect

        class ObjectEncoder(json.JSONEncoder):
            def default(self, obj):
                if hasattr(obj, "to_json"):
                    return self.default(obj.to_json())
                elif hasattr(obj, "__dict__"):
                    d = dict(
                        (key, value)
                        for key, value in inspect.getmembers(obj)
                        if not key.startswith("__")
                        and not inspect.isabstract(value)
                        and not inspect.isbuiltin(value)
                        and not inspect.isfunction(value)
                        and not inspect.isgenerator(value)
                        and not inspect.isgeneratorfunction(value)
                        and not inspect.ismethod(value)
                        and not inspect.ismethoddescriptor(value)
                        and not inspect.isroutine(value)
                    )
                    return self.default(d)
                return obj

        sched_info = {
            'slots': self.get_all_slots(),
            'total_cost': self.total_cost,
            'trains_visited_ever': list(self.trains_visited_ever),
            'trains_visited_once': list(self.trains_visited_once),
            'trains_visited_twice': list(self.trains_visited_twice)
        }
        return json.dumps(sched_info, cls=ObjectEncoder, indent=2)

    def to_dict(self):
        return json.loads(self.to_json())

class Solver:
    _STORAGE_AREA_TRACK = -1

    def __init__(self,
       train_count ,             # |I| in formulas
       earliest_arrivals,        # e in formulas
       latest_departures,        # l in formulas
       cargos,                   # list of cargos. each cargo is a dict (source_train_id: i, target_train_id: j, box_count: Aij)
                                 #   in formulas it's NxN matrix "A", where Aij = number of containers that train i carry for train j.
                                 #   for programming convenience, we use list of cargos instead of matrix

       track_count ,             # G in formulas
       max_slot_count ,          # MY PARAM. T in formulas + some limit
       track_to_track_move_costs, # c in formulas. Matrix GxG. c[ij] = move cost between track i and j.
       track_to_storage_move_costs, # each track -> storage area move cost. Tuple of length G
       storage_to_track_move_costs, # storage area -> each track move cost. Tuple of length G

       objective_1_weight,       # a1 in formula. Weight of objective 1 - min revisit.
       objective_2_weight,       # a2 in formula. Weight of objective 2 - min split move cost.
       objective_3_weight,       # a3 in formula. Weight of objective 3 - min direct move cost.

       solution_method,          # 'exact' or 'beam_search'
       beam_width = None,        # if move_precisely=False, then beam_width is the level of solution precision
       arrange_trains = True, # if True, trains in each slot will be arranged optimally (but solution time increases with factorial(track_count))

       time_limit = 5,           # max time (in seconds) to solve task,
       **kwargs
       ):
        self._params = dict(
            train_count = train_count,
            earliest_arrivals = earliest_arrivals,
            latest_departures = latest_departures,
            track_count = track_count,
            max_slot_count = max_slot_count,
            track_to_track_move_costs = track_to_track_move_costs,
            track_to_storage_move_costs = track_to_storage_move_costs,
            storage_to_track_move_costs = storage_to_track_move_costs,
            objective_1_weight = objective_1_weight,
            objective_2_weight = objective_2_weight,
            objective_3_weight = objective_3_weight,
            solution_method = solution_method,
            beam_width = beam_width if solution_method == 'beam_search' else 99999999,
            arrange_trains = arrange_trains,
            time_limit = time_limit
        )
        # TODO: validate input params (dimensions of matrixes/lists). I.g., earliest_arrivals should be of length = train_count
        #       and max_slot_count >= train_count/track_count (7 trains, 2 tracks and only 3 max_slot_count is incorrect)

        self._storage_area = StorageArea()
        self._trains = [Train(id=train_id, cargos=[]) for train_id in range(train_count)]
        self.moves_to_perform = [] # { (source_train, target_train):{'status':..., 'cargo':...} }
        self.cargos = {}

        inactive_train_ids = [t.id for t in self._trains]
        for cargo in cargos:
            source = self._get_train_by_id(cargo['source_train_id'])
            target = self._get_train_by_id(cargo['target_train_id'])
            if source == target:
                raise InputError('Invalid cargo {}-{}: source and target are the same train'\
                                 .format(source.id, target.id))
            if (source, target) in self.cargos.keys():
                raise InputError('Cargo {}-{} duplicates. Please, remove duplicates!'\
                                 .format(source.id, target.id))

            self.moves_to_perform.append((source,target))
            self.cargos[(source,target)] = Cargo(id='{}-{}'.format(source.id,
                                                                    target.id),
                                                  source=source, target=target,
                                                  box_count=cargo['box_count'])
            if source.id in inactive_train_ids:
                inactive_train_ids.remove(source.id)
            if target.id in inactive_train_ids:
                inactive_train_ids.remove(target.id)

        if inactive_train_ids:
            raise InputError('There is no cargo from/to trains #{}. Please assign cargo(s) to them'\
                             .format(','.join([str(i) for i in inactive_train_ids])))

        if MODE == 'debug':
            sched = Schedule(None,None,moves_to_perform = self.moves_to_perform)
            for train in self._trains:
                log('{}. Sources: {}. Targets: {}'.format(train,
                                                          sched.get_train_sources(train),
                                                          sched.get_train_targets(train)),
                    level='debug', type='sched')

    def _get_train_by_id(self, id):
        # we assume that _trains is list like [Train0,Train1,Train2,...]
        return self._trains[id]

    # STUB
    def solve(self):
        @timeout(self._params['time_limit'])
        def _solve():
            sched = Schedule(None,None, moves_to_perform = self.moves_to_perform)
            final_scheds = []

            start_time = timer()
            self._generate_child_schedules(sched, final_scheds)

            best_sched = min(final_scheds,key=lambda sched: sched.total_cost) if final_scheds else None

            worktime = timer() - start_time
            result = {
                'human_readable': 'Best sched is {}. Solution took {}s. {} scheds generated'.\
                                format(best_sched, worktime, len(final_scheds)),
                'time': worktime,
                'scheds_count': len(final_scheds),
                'best_sched' : best_sched,
            }
            log('Solved TSY: ' + result['human_readable'], file = None)
            # log(result, level='debug')

            log(result['best_sched'], level='info')
            return result

        solution = _solve()
        if not solution['best_sched']:
            raise NoSolutionError('No solution found')
        return solution


    def _is_sched_complete(self, sched):
        return sched.trains_visited_ever == set(self._trains)

    def _generate_child_schedules(self, sched, result_container):
        log('', type='sched', level='debug')
        log('*'*sched.slot_count, prefix='  '*sched.slot_count, type='sched', level='debug')
        if self._is_sched_complete(sched):
            if self._is_complete_sched_valid(sched):
                result_container.append(sched)
            else:
                log('! got invalid complete sched', prefix='  '*sched.slot_count, type='sched', level='debug')

            log('FULL_SCHED '+str(sched), prefix='  '*sched.slot_count, type='sched', level='debug')
            return

        log('Parent: '+str(sched), prefix='  '*sched.slot_count, type='sched', level='debug')
        slots = self._get_new_slots(sched)

        if len(slots) == 0:
            log('!!! There\'s no new slots. Sched build is stopped !',
                prefix='  '*sched.slot_count, type='sched', level='warning')


        log('Will add slots: ', prefix='  '*sched.slot_count, type='sched', level='debug')

        # beam search implementation
        # sort slots by train count DESC (try to take slots with no trains cut), then by cost ASC
        slots.sort(key=lambda slot: (-len(slot.trains), slot.cost))
        for slot in slots[:self._params['beam_width']]:
            child_sched = Schedule(sched, slot)
            if self._is_sched_valid(child_sched):
                sched.children.append(child_sched)
                log(slot, prefix='  '*child_sched.slot_count, type='sched', level='debug')

        for child in sched.children:
            self._generate_child_schedules(child, result_container)

    def _can_train_be_in_next_slot(self, train, sched):
        """ This function only looks at:
           - train arrive-departure interval
           - whether train already visited the yard twice

           However, not all trains that visited the yard once can revisit at next slot.
                    They can revisit only after all their sources will arrive to yard
                    (also train can revisit in the same slot with its last source trains)
        """
        t = sched.slot_count # next slot number
        return (# slot is in train's [earliest arrival;latest departure] interval
                self._params['earliest_arrivals'][train.id] <= t <= self._params['latest_departures'][train.id]
                and
                # train hasn't already revisited the yard
                train not in sched.trains_visited_twice)

    def _can_train_be_in_slot(self, train, trains, sched):
        """ When we call this function we already know train neighbors in slot
        """

        # if train is going to revisit,
        #   all its source trains must already arrive
        #   (or arrive in the same slot as train)
        trains_visited_or_will_visit_now = sched.trains_visited_ever.union(set(trains))
        if train in sched.trains_visited_once:
            if not sched.get_train_sources(train, all_sources=True).issubset(trains_visited_or_will_visit_now):
                log('!{} is invalid because {} is going to revisit but not all its sources have visited'\
                       .format(set(trains),train),
                     level='debug', type='sched', prefix='  '*sched.slot_count)
                return False

        # there's no sense for train to visit if it will not pickup or deliver any cargo
        # train has nothing to delived   # and no trains in next slot carry cargo for train
        if not sched.get_train_targets(train) and not sched.get_train_sources(train).intersection(set(trains)) \
           and not sched.storage_area.has_cargo_for(train): # and storage area has no cargo for train
            log('!{} is invalid because {} will not pickup or deliver any cargo'\
                   .format(set(trains),train),
                 level='debug', type='sched', prefix='  '*sched.slot_count)
            return False

        return True

    def _can_train_set_form_next_slot(self, trains, sched):
        # don't allow all trains to stay for 2 slots
        if sched.last_slot and set(trains) == set(sched.last_slot.trains):
            log('!{} is invalid because it contains the same trains as last slot'.\
                 format(set(trains)),
                 level='debug', type='sched', prefix='  '*sched.slot_count)
            return False

        # if we are building a last slot
        # we require that, after adding 'trains', resulting full sched will be complete (whether it will contain all trains)
        if sched.slot_count == self._params['max_slot_count']-1 and \
           sched.trains_visited_ever.union(set(trains)) != set(self._trains):
            log('!{} is invalid because full sched will not contain all trains!'\
                   .format(set(trains)),
                 level='debug', type='sched', prefix='  '*sched.slot_count)
            return False

        # check that schedule will be able to has all trains at the end (all trains will visit at least once)
        # for that:
        #  1. assume that we will add current trains to sched
        #  2. count max number of unique trains that will be in sched in future (num of unhold positions)
        #  3. compare it with number of trains that will be absent in sched after we will add current trains
        #     (all trains - trains present in sched - current 'trains')
        #
        # if, i.g., there're 6 positions are unhold (there're 2 slots left * 3 tracks) but 7 trains haven't visited yet,
        #   full sched will NOT contain all trains (at least 1 train will be missed)
        unhold_positions_num = (self._params['max_slot_count'] - sched.slot_count - 1) * self._params['track_count'] # -1 because we imaginary add 'trains'
        unvisited_trains_num = len(set(self._trains).difference(sched.trains_visited_ever.union(trains))) # sched.trains_visited_ever.union(trains)) because we imaginary add 'trains'
        # log('i ({} unvisited trains ; {} unhold positions)'.format(unvisited_trains_num, unhold_positions_num),
        #      level='debug', type='sched', prefix='  '*sched.slot_count)
        if unvisited_trains_num > unhold_positions_num:
            log('!{} is invalid because full sched will not be able to contain all trains({} unvisited trains > {} unhold positions)'\
                   .format(set(trains), unvisited_trains_num, unhold_positions_num),
                 level='debug', type='sched', prefix='  '*sched.slot_count)
            return False

        return True

    def _get_revisit_cost(self):
        return self._params['objective_1_weight']

    def _get_move_cost(self, source_track, target_track, box_count):
        if source_track == target_track:
            raise ValueError('Source track and target track must not be equal!')

        if source_track == self._STORAGE_AREA_TRACK:
            return box_count*self._params['storage_to_track_move_costs'][target_track]
        elif target_track == self._STORAGE_AREA_TRACK:
            return box_count*self._params['track_to_storage_move_costs'][source_track]
        else:
            return box_count*self._params['track_to_track_move_costs'][source_track][target_track]

    def _get_new_slots(self, sched):
        avail_trains = [t for t in self._trains if self._can_train_be_in_next_slot(t,sched)]
        log('avail_trains: {}'.format(avail_trains), prefix='  '*sched.slot_count,
            level='debug', type='sched')

        slots = []
        for trains in itertools.combinations(avail_trains, self._params['track_count']):
            # cut trains which cant be in this slot
            trains = [t for t in trains if self._can_train_be_in_slot(t, trains, sched)]

            # about 'Slot(trains) in slots': when we cut trains which cant be in this slot,
            #  from 2 slots (0,1,2) and (0,1,3) we can get (0,1) and (0,1). So we sure that slots dont duplicate
            if not trains or not self._can_train_set_form_next_slot(trains, sched) or Slot(trains) in slots:
                continue
            slot = self._form_arranged_slot(sched, trains) if self._params['arrange_trains'] \
                   else self._form_slot(sched, trains)
            slots.append(slot)

        return slots

    def _form_arranged_slot(self, sched, train_set, greedy=True):
        # form slot where trains are arranged optimally
        # TODO: greedy
        # if self._params['greedy']:
        #     trains = [random.choice(train_set)]
        #     free_trains = set(train_set) - set(trains)
        #     for i in range(len(train_set)-1):
        #         slots = map(lambda t: (t, self._form_slot(sched, trains+[t])), free_trains)
        #         best_free_train = min(slots, key=lambda slot: slot[1].cost)[0]
        #         trains.append(best_free_train)
        #         free_trains.remove(best_free_train)
        #     return self._form_slot(sched, trains)
        # else:
            slots = []
            for trains in itertools.permutations(train_set):
                slots.append(self._form_slot(sched, trains))
            return min(slots,key=lambda slot: slot.cost)

    def _form_slot(self, sched, trains):
        slot = Slot(trains)

        log('== trains: {}'.format(trains), prefix='  '*sched.slot_count,
                    level='debug', type='sched')
        # TODO NEXT: count cost + choose cheapest scheds
        for track,train in enumerate(slot.trains):
            # register revisit
            if train in sched.trains_visited_once:
                revisit_cost = self._get_revisit_cost()
                slot.revisits.append({'train_id':train.id, 'cost': revisit_cost})
                slot.cost += revisit_cost

            # unload boxes
            for target in sched.get_train_targets(train):

                if target in slot.trains:
                    move_type,target_track = (Schedule.MOVE_TYPE_TRAIN_TRAIN,slot.trains.index(target))
                else:
                    move_type,target_track = (Schedule.MOVE_TYPE_TRAIN_STORAGE,self._STORAGE_AREA_TRACK)

                if sched._can_move(source = train, target = target, move_type=move_type): # IMPLEMENT
                    cargo = self.cargos[(train, target)]
                    move_cost = self._get_move_cost(track, target_track, cargo.box_count)
                    slot.cost += move_cost


                    slot.moves.append({'source': train, 'target': target,
                                       'type': move_type, 'cost': move_cost, 'cargo': cargo})

            # load boxes from storage area. no need to load them from neightbor trains 'cause this will be done in above code for sources
            move_type,source_track = Schedule.MOVE_TYPE_STORAGE_TRAIN,self._STORAGE_AREA_TRACK
            if sched.storage_area.has_cargo_for(train):
                for cargo in sched.storage_area.get_cargos_for(train):
                    if sched._can_move(source = cargo.source, target = train,move_type = move_type): # TODO: убрать эту проверку
                        move_cost = self._get_move_cost(source_track, track,
                                                        cargo.box_count)
                        slot.cost += move_cost
                        slot.moves.append({'source': cargo.source, 'target': train,
                                           'type': move_type, 'cost': move_cost, 'cargo': cargo})
        return slot

    def validate_sched(self, sched):
        pass

    def _is_sched_valid(self,sched):
        return True

    def _is_complete_sched_valid(self, sched):
        all_moves_complete = (set(sched.moves_info.values()) == {Schedule.MOVE_COMPLETED})
        return (all_moves_complete and len(sched.storage_area._cargos) == 0)
        # try:
        #     self.validate_sched(sched)
        #     return True
        # except ValueError:
        #     return False

def solve(params):
    Solver(**params).solve()
