#!/usr/bin/env python

import unittest

from tsy import *
import tsy
from random import randint
import random
from copy import copy,deepcopy

from math import ceil as round_to_next_int
import itertools

def log(message, level='info', prefix = '', type=''):

    if type.startswith('sched'):
        return

    if isinstance(message,list) or isinstance(message,dict):
        message = pformat(message)
    else:
        message = str(message)

    if level == 'warning':
      level = '! Warning !'
    if level == 'error':
      level = '!!! ERROR !!! '

    # datetime.datetime.today().isoformat()
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+' ('+level+')\t'+prefix+message)
    if '\n' in message:
        print('\n')

def get_dummy_cargo():
    return Cargo(id=randint(1001,1999),
        source='train{}'.format(randint(100,1000)),
        target='train{}'.format(randint(100,1000)),
        box_count=randint(1,10))

def get_dummy_train():
    random.seed()
    return Train(id=randint(100,1000), cargos=[get_dummy_cargo()])

def get_dummy_slot(track_count):
    slot = Slot([get_dummy_train() for i in range(track_count)])
    slot.cost = randint(10,300)
    return slot

class StorageAbstractTestCase(unittest.TestCase):
    def setUp(self):
        self.storage = StorageAbstract([Cargo(id=1, source='train11', target='train12', box_count=1),
                                               Cargo(id=2, source='train21', target='train22', box_count=2)])

    def test_add_cargo(self):
        self.storage.add_cargo(Cargo(id=3, source='train31', target='train32', box_count=3))
        self.assertEqual(len(self.storage._cargos), 3)

    def test_get_cargo_for(self):
        cargos = self.storage.get_cargos_for('train22')
        self.assertIsInstance(cargos.pop(), Cargo)

    def test_has_cargo_for_valid_target(self):
        self.assertTrue(self.storage.has_cargo_for('train12'))

    def test_has_no_cargo_for_invalid_target(self):
        self.assertFalse(self.storage.has_cargo_for('non-existing target'))

    def test_remove_cargo(self):
        cargo = self.storage.get_cargos_for('train22').pop()
        self.storage.remove_cargo(cargo)
        self.assertFalse(self.storage.has_cargo_for('train22'))
        self.assertEqual(len(self.storage._cargos), 1)


class TrainTestCase(unittest.TestCase):
    def test_create(self):
        get_dummy_train()

class SlotTestCase(unittest.TestCase):
    def test_error_when_init_with_duplicated_trains(self):
        t1 = Train(id=1, cargos=[])
        t2 = Train(id=1, cargos=[])
        with self.assertRaises(ValueError):
            Slot([t1,t2])

class ScheduleTestCase(unittest.TestCase):
    def setUp(self):
        self.root = Schedule(None,None, moves_to_perform = {})

    def test_add_slot(self):
        slot = get_dummy_slot(3)
        # no train visited yet
        self.assertEqual(self.root.trains_visited_ever, set())
        self.assertEqual(self.root.trains_visited_once, set())
        self.assertEqual(self.root.trains_visited_twice, set())

        # all trains visited once
        sched1 = Schedule(self.root, slot)

        # check that root did not changed
        self.assertEqual(self.root.trains_visited_ever, set())
        self.assertEqual(self.root.trains_visited_once, set())
        self.assertEqual(self.root.trains_visited_twice, set())

        for train in slot.trains:
            self.assertIn(train,sched1.trains_visited_ever)
            self.assertIn(train,sched1.trains_visited_once)
            self.assertNotIn(train,sched1.trains_visited_twice)

        # all trains visited twice
        sched2 = Schedule(sched1, deepcopy(slot))
        for train in slot.trains:
            self.assertIn(train,sched2.trains_visited_ever)
            self.assertNotIn(train,sched2.trains_visited_once)
            self.assertIn(train,sched2.trains_visited_twice)
            # check that sched1 did NOT change
            self.assertIn(train,sched1.trains_visited_once)
            self.assertNotIn(train,sched1.trains_visited_twice)

        # check error is raised when trying to add already revisited trains
        with self.assertRaises(ValueError):
           sched3 = Schedule(sched2, deepcopy(slot))

class SolverTestCaseAbstract(unittest.TestCase):
    def setUp(self):
        self._cargos = ({'source_train_id': 0,
                         'target_train_id': 1,
                         'box_count': 101},
                        {'source_train_id': 1,
                         'target_train_id': 2,
                         'box_count': 102},
                        {'source_train_id': 2,
                         'target_train_id': 1,
                         'box_count': 1021},
                        {'source_train_id': 2,
                         'target_train_id': 0,
                         'box_count': 103,},
                        {'source_train_id': 1,
                         'target_train_id': 3,
                         'box_count': 104})
        train_count = 4
        max_slot_count = 3
        self.solver = Solver(train_count = train_count,
            earliest_arrivals = [0]*train_count,
            latest_departures = [max_slot_count-1]*train_count, # latest is max timeslot
            cargos = self._cargos,
            track_count = 2,
            max_slot_count = max_slot_count,
            track_to_track_move_costs = [(1,1),(1,1)],
            track_to_storage_move_costs = (2,2),
            storage_to_track_move_costs = (3,3),
            objective_1_weight = 1,
            objective_2_weight = 1,
            objective_3_weight = 1,
            solution_method = 'exact'
            )

class SolverDummyTestCase(SolverTestCaseAbstract):
    def setUp(self, *args, **kwargs):
        log('=== DUMMY TEST ===',level='debug')
        tsy.MODE = 'no_debug'
        super(self.__class__, self).setUp(*args, **kwargs)
        self.solver._get_new_slots = self.get_dummy_new_slots
        self.solver._is_sched_valid = lambda sched: True
        self.solver._is_complete_sched_valid = lambda sched: True

        min_needed_slot_count = round_to_next_int(self.solver._params['train_count']/self.solver._params['track_count'])
        self.solver._is_sched_complete = lambda sched: sched.slot_count == min_needed_slot_count

    def get_dummy_new_slots(self,sched):
        dummy_new_slots = [get_dummy_slot(2),
                           get_dummy_slot(2)]
        # dummy_new_slots = [get_dummy_slot(3),
        #                    get_dummy_slot(3),
        #                    get_dummy_slot(3)]
        return dummy_new_slots

    def test_solution_is_of_right_class(self):
        solution = self.solver.solve()['best_sched']
        self.assertIsInstance(solution, Schedule)

    def test_can_train_be_in_next_slot(self):
        train0 = self.solver._get_train_by_id(0) # train 0 will be avaliable for 2 slots

        train1 = self.solver._get_train_by_id(1)
        self.solver._params['earliest_arrivals'][1] = 999 # train 1 will be never avaliable

        train2 = self.solver._get_train_by_id(2) # train 2 will be avaliable only for first slot
        self.solver._params['latest_departures'][2] = 0 # train 3 can never be in next slot

        train3 = self.solver._get_train_by_id(3)
        self.solver._params['earliest_arrivals'][3] = 1 # train 3 will be avaliable only for second slot

        # 1 slot (slot #0)
        sched0 = Schedule(None,None, moves_to_perform = self.solver.moves_to_perform)
        self.assertTrue(self.solver._can_train_be_in_next_slot(train0, sched0))
        self.assertFalse(self.solver._can_train_be_in_next_slot(train1, sched0))
        self.assertTrue(self.solver._can_train_be_in_next_slot(train2, sched0))
        self.assertFalse(self.solver._can_train_be_in_next_slot(train3, sched0))

        # 2 slots (slots #0, #1)
        sched1 = Schedule(sched0, get_dummy_slot(2))
        self.assertTrue(self.solver._can_train_be_in_next_slot(train0, sched1))
        self.assertFalse(self.solver._can_train_be_in_next_slot(train1, sched1))
        self.assertFalse(self.solver._can_train_be_in_next_slot(train2, sched1))
        self.assertTrue(self.solver._can_train_be_in_next_slot(train3, sched1))

    def test_trains_created_correctly(self):
        pass
        # for c in self._cargos:
        #     source = self.solver._get_train_by_id(c[0])
        #     target = self.solver._get_train_by_id(c[1])
        #     self.assertTrue(source.has_cargo_for(target))
        #     self.assertIn(target,source.initial_targets)
        #     self.assertIn(source,target.initial_sources)

        # # hardcode test
        # train1 = self.solver._get_train_by_id(1)
        # self.assertTrue(train1.has_cargo_for(self.solver._get_train_by_id(2)))
        # self.assertTrue(train1.has_cargo_for(self.solver._get_train_by_id(3)))
        # self.assertFalse(train1.has_cargo_for(self.solver._get_train_by_id(0)))

        # # hardcode test
        # train0 = self.solver._get_train_by_id(0)
        # train1 = self.solver._get_train_by_id(1)
        # self.assertIn(train1,train0.initial_targets) # train 0 carry cargo for train 1
        # self.assertIn(train0,train1.initial_sources) # train 1 will accept cargo from train 1
        # self.assertNotIn(train0,train1.initial_targets) # but not
        # self.assertNotIn(train1,train0.initial_sources) #   vice versa

    def test_move_cargo(self):
        pass
        # train1 = self.solver._get_train_by_id(1)
        # train3 = self.solver._get_train_by_id(3)
        # self.solver._move_cargo(train1,train3) # train 1 has box for train 3
        # with self.assertRaises(ValueError):
        #     self.solver._move_cargo(train3,train1) # but not vice versa!

        # self.solver._move_cargo(train1, self.solver._storage_area)
        # self.solver._move_cargo(self.solver._storage_area, train3)

    def test_get_move_cost(self):
        with self.assertRaises(ValueError):
            self.solver._get_move_cost(1,1,1)

class SolverRealTestCase(SolverTestCaseAbstract):
    def setUp(self, *args, **kwargs):
        log('=== REAL TEST ===',level='debug')
        tsy.MODE = 'debug'
        super(self.__class__, self).setUp(*args, **kwargs)

    def test_solution_is_valid(self):
        solution = self.solver.solve()['best_sched']
        self.assertTrue(self.solver._is_sched_valid(solution))
        # try:
        #     self.solver.validate_sched(solution)
        # except ValueError as e:
        #     self.fail('Solution {} is invalid:{}'.format(solution, str(e)))

    def test_can_train_be_in_next_slot(self):

        train0 = self.solver._get_train_by_id(0)
        train1 = self.solver._get_train_by_id(1)
        train2 = self.solver._get_train_by_id(2)
        train3 = self.solver._get_train_by_id(3)

        # empty sched
        sched0 = Schedule(None,None, moves_to_perform = self.solver.moves_to_perform)

        # test 1. all train combinations are avaliable at the beginning
        for trains in itertools.combinations(self.solver._trains, 2):
            self.assertTrue(self.solver._can_train_set_form_next_slot(trains, sched0))

        # 1 slot in sched
        slot0 = Slot([train1, train3])
        sched1 = Schedule(sched0, slot0)

        # test 2. same trains can't be in next slot
        self.assertFalse(self.solver._can_train_set_form_next_slot(slot0.trains, sched1))

        # test 3. trains that were absent in slot0 can be in next slot
        free_trains = [train0, train2]
        self.assertTrue(self.solver._can_train_set_form_next_slot(free_trains, sched1))

        self.solver._params['max_slot_count'] = 2
        # tests 4.1. trains = [1 old train with all sources arrived, 1 new train]:
        #  but max_slot_count = 2 => can't be
        self.assertFalse(self.solver._can_train_set_form_next_slot([train3,train2], sched1))

        # tests 4.2. max_slot_count = 3 & trains = [1 old train, 1 new train]
        self.solver._params['max_slot_count'] = 3
        # test 4.2.1. trains = [1 old train with all sources arrived, 1 new train]:
        #   train 3 can revisit 'cause source train 1 already arrived
        #     => can be
        self.assertTrue(self.solver._can_train_set_form_next_slot([train3,train2], sched1))

        # test 4.2. trains = [1 old train with NOT all sources arrived, 1 new train]:
        #   train 1 can't revisit 'cause source train 0 haven't arrived yet
        #     => can't be
        self.assertFalse(self.solver._can_train_be_in_slot(train1, [train1,train2], sched1))

if __name__ == "__main__":
    unittest.main()
