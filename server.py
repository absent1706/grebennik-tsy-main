import os
from datetime import datetime
from flask import Flask, request, flash, url_for, redirect, \
     render_template, abort, send_from_directory

app = Flask(__name__)
app.config.from_pyfile('flaskapp.cfg')


import json
from pprint import pprint
import os, sys, traceback
from tsy import Solver, ClientError, TimeoutError, NoSolutionError

dummy_params = """{"train_count":4,"earliest_arrivals":[-1,-1,-1,-1],"latest_departures":[999,999,999,999],"cargos":[{"source_train_id":0,"target_train_id":1,"box_count":101},{"source_train_id":1,"target_train_id":2,"box_count":102},{"source_train_id":2,"target_train_id":1,"box_count":1021},{"source_train_id":2,"target_train_id":0,"box_count":103},{"source_train_id":1,"target_train_id":3,"box_count":104}],"track_count":2,"max_slot_count":4,"track_to_track_move_costs":[[null,1],[1,null]],"storage_to_track_move_costs":[1,2],"track_to_storage_move_costs":[1,2],"objective_1_weight":3,"objective_2_weight":2,"objective_3_weight":1,"solution_method":"beam_search","beam_width":2,"time_limit":10,"arrange_trains":true}"""

@app.route("/")
def home():
  return render_template('home.html')


@app.route("/solve", methods=['POST'])
def solve():
    params = request.json['params']
    if int(params['time_limit']) > 60:
       return json.dumps({'client_error': 'Max time limit for web app is 60s'})
    try:
      return json.dumps(solve(params))
    except ClientError, e:
      return json.dumps({'client_error': str(e)})
    except TimeoutError, e:
      return json.dumps({'client_error': str(e)})
    except NoSolutionError, e:
      return json.dumps({'client_error': str(e)})
    except Exception:
      exc_type, exc_value, exc_traceback = sys.exc_info()
      error = traceback.format_exc().splitlines()
      # import pdb; pdb.set_trace()
      return json.dumps({'error': error})

@app.route("/solve-dummy", methods=['GET', 'POST'])
def solve_dummy():
    params = json.loads(dummy_params)
    return json.dumps(solve(params))

def solve(params):
  # pprint(params)
  os.environ['TSY_MODE'] = 'test'
  solver = Solver(**params)
  # import pdb; pdb.set_trace()
  solution = solver.solve()
  solution['best_sched'] = solution['best_sched'].to_json()
  return solution

if __name__ == '__main__':
    app.run(debug=True)
